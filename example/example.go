package main

import (
	"fmt"
	"github.com/oblitum/pygments"
	"log"
)

func main() {
	p, err := pygments.New(
		pygments.Executable("/usr/bin/pygmentize"),
		pygments.Formatter("terminal"),
	)
	if err != nil {
		panic(err)
	}

	pygmentized, err := p.Pygmentize("example.go")
	if err != nil {
		panic(err)
	}

	fmt.Print(pygmentized)
	fmt.Print("\n\n\n\n\n")
	fmt.Print(tryPygmentizeWith256AndRollback(p, "example.go"))
	fmt.Print("\n\n\n\n\n")

	pygmentized, err = p.Pygmentize("example.go")
	if err != nil {
		panic(err)
	}

	fmt.Print(pygmentized)
}

func tryPygmentizeWith256AndRollback(p *pygments.Pygments, path string) string {
	rollback, err := p.Configure(pygments.Formatter("terminal256"))
	defer p.Configure(rollback)

	if err != nil {
		log.Println("Could not setup terminal256 formatter:", err)
		return ""
	}

	pygmentized, err := p.Pygmentize(path)

	if err != nil {
		log.Println("Error pygmentizing with terminal256 formatter:", err)
		return ""
	}
	return pygmentized
}
